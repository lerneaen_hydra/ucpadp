function [y_out, t_out] = rk4(odefun, y0, N, t_range, varargin)
% RK4 Efficient 4th order runge-kutta ODE solver
%	Y = RK4(ODEFUN, Y0, N, T_RANGE) efficiently solves a given ODE function
%	for an initial condition(s) y0, number of steps N, and time range
%	t_range.
%	[Y, T] = RK4(ODEFUN, Y0, N, T_RANGE, 'log') acts as above, but stores
%	intermediate values of Y in T_RANGE and returns them in Y. This will
%	run much more slowly than the above method and is only intended for
%	performnace-insensitive applications.
% Usage example:
%	y = rk4(@(t, y) y, 1, 1e3, [0, 1])
% solves the ODE y' = y for
%	y(0) = 1,
%	1000 steps,
%	and time range [0, 1].
% Multi-state multi-IC example:
% Assume we want to solve y'' = y for initial conditions y0 = [1, 0; 0.2, 0.2],
% i.e. initial conditions y01 = 1, y01' = 0, and y02 = 0.2, y02' = 0.2.
% This can be solved using:
% 
% y0 = [1, 0; 0.2, 0.2];
% [y, t] = rk4(@(t, y) harm_osc(y), y0, 1e3, [0, 2*pi], 'log');
% y = cat(3, y{:});
% 
% y1 = squeeze(y(1,:,:));
% y2 = squeeze(y(2,:,:));
% 
% clf;
% plot(t, [y1; y2]);
% 
% legend('IC1 y', 'IC1 y''', 'IC2 y', 'IC2 y''');
% 
% function dydt = harm_osc(y)
% 	y1 = y(:,1);
% 	y2 = y(:,2);
% 	dydt = [-y2, y1];
% end
%
% y0 may be a row vector (for systems with multiple states) and/or a column
% vector/matrix (for testing multiple y0 efficiently).

h = (t_range(2) - t_range(1))/N;

logmode_default = '';
logmode = logmode_default;
if nargin > 4
	logmode = varargin{1};
end

if(strcmp(logmode, 'log'))
	y_out{1} = y0;
	t_out = [];
else
	t_out = t_range(1);
end

y = y0;

for n=0:(N-1)
	% Get the time corresponding to the current subsample
	alpha = n/(N);
	t = (1-alpha) * t_range(1) + alpha * t_range(2);
	
	% Generate increments
	k1 = odefun(t, y);
	k2 = odefun(t + h/2, y + h*k1/2);
	k3 = odefun(t + h/2, y + h*k2/2);
	k4 = odefun(t+h, y + h*k3);
	
	% Update state
	y = y + h/6 * (k1 + 2*k2 + 2*k3 + k4);
	
	% Log result if requested
	if(strcmp(logmode, 'log'))
		y_out{end + 1} = y; %#ok<AGROW>
		t_out(end+1) = t;	%#ok<AGROW>
	end
end

if(~strcmp(logmode, 'log'))
	y_out = y;
	t_out = t_range(2);
else
	t_out(end+1) = t_range(2);
end

end

