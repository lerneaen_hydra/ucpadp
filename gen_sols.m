clc
clear variables
close all
clearAllMemoizedCaches
format short eng

addpath(['.', filesep, 'dpm', filesep, 'dpm']);
addpath(['.', filesep, 'export_fig']);
addpath(['.', filesep, 'hatchfill2']);

% Distance between evaulated points in state space
% First, generate a fairly coarse solution to get fast results.
% Then, generate more accurate solutions for inclusion in the paper.
delta_xs_inv = {[0.15; 0.15], [0.1; 0.1], [0.05; 0.05]};
% Use a denser discretization for the constant-angle problem because we can
% afford to do this as the search space is smaller.
delta_xs_ang = {[0.1; 0.1], [0.05; 0.05], [0.02; 0.02]};

%%
for k=1:length(delta_xs_inv)
    % Inverted pendulum setup
    fprintf('Solving inverted pendulum problem\n');
    delta_x = delta_xs_inv{k};
    fprintf('Solving with state resolution %s\n', mat2str(delta_x));
    
    % Set up model constants
    model_param.m = 1;
    model_param.l = 1;
    model_param.g = 1;
    model_param.k = 0;
    
    % Generate DP problem
    dp_param = setup_dp(delta_x, 'inverted');
    
    % Create cost function, penalize non-vertical non-stationary states. Add
    % small penalty to control to give nonsingular control near equilibrium
    cfun_inverted = @(x1, x2, u) double(~all([
        abs(x1 - pi) < (2*(dp_param.prb.X_h(1) - dp_param.prb.X_l(1)) ./ dp_param.prb.N_x_grid(1)), ...
        abs(x2)      < (2*(dp_param.prb.X_h(2) - dp_param.prb.X_l(2)) ./ dp_param.prb.N_x_grid(2))
        ], 2)) + 1e-6 * u.^2;
    
    % Apply UCPADP method
    res_inverted{k} = gen_ucpadp_sol(dp_param, model_param, 'costfun', cfun_inverted); %#ok<*SAGROW>
    
    % Generate reference solution to inverted pendulum problem
    [foo, bar, baz] = gen_ref_sol(dp_param, model_param, cfun_inverted, res_inverted{k}, 40);
    res_inverted_ref{k} = foo;
    map_inverted_ref{k} = bar;
    cost_inverted_comp{k} = baz;    
end

fprintf('Inverted pendulum cost comparison:\n');
for k=1:length(delta_xs_inv)
    fprintf('delta-x: %s\n', mat2str(delta_xs_inv{k}));
    disp(cost_inverted_comp{k}.summary);
end

%%
for k=1:length(delta_xs_ang)
    % Constant angle pendulum problem
    fprintf('Solving constant-angle problem\n');
    delta_x = delta_xs_ang{k};
    fprintf('Solving with state resolution %s\n', mat2str(delta_x));
    
    % Set up model constants
    model_param.m = 1;
    model_param.l = 1;
    model_param.g = 1;
    model_param.k = 1;
    
    % Generate DP problem
    dp_param = setup_dp(delta_x, 'angle');
    
    % Target equilibrium state
    x1_ref = 0.5;
    
    % Nominal value of lambda found be setting derivative of equilibrium
    % cost to zero and solving for lambda.
    lambda = -2 * model_param.m.^2 * model_param.g.^2 * model_param.l.^2 * sin(x1_ref) * cos(x1_ref);
    
    cfun_angle = @(x1,x2,u) u.^2 + lambda * x1;
    
    % Apply UCPADP method
    res_angle{k} = gen_ucpadp_sol(dp_param, model_param, 'costfun', cfun_angle);
    
    % Generate reference solution to constant-angle problem
    [foo, bar, baz] = gen_ref_sol(dp_param, model_param, cfun_angle, res_angle{k}, 100);
    res_angle_ref{k} = foo;
    map_angle_ref{k} = bar;
    cost_angle_comp{k} = baz;
    %%
    % Compute total cost for solution with swept horizon
    x0s = map_angle_ref{k}{1}(1).x;
    
    horz_sweep = 5:res_angle{k}.N_m*2;
    c_sweep = zeros(size(x0s, 1), length(horz_sweep));
    
    for l = 1:length(horz_sweep)
        fprintf('Generating swept-horizon solution, horizon %d\n', horz_sweep(l));
        % Extract the computed optimal controls and associated states
        u_o = map_angle_ref{k}{1}(end-horz_sweep(l)).u_o;
        x = map_angle_ref{k}{1}(end-horz_sweep(l)).x;
        
        % Reshape them for a format supported by griddedinterpolant
        grid_x = [length(unique(x(:,1))), length(unique(x(:,2)))];
        u_o = reshape(u_o, grid_x);
        for i = 1:size(x, 2)
           temp{i} = reshape(x(:,i), grid_x);
        end
        
        % Generate the control law associated with a problem horizon l
        u_law = griddedInterpolant(temp{:}, u_o);
        
        % Compute the cost of applying the control law over a long horizon
        x = x0s;
        c_inst = zeros(size(x, 1), res_angle{k}.N_m*10);
        inp.m = model_param;
        inp.arg.costfun = cfun_angle;
        for m=1:size(c_inst, 2)
           [x, c_inst(:,m)] = model(x, u_law(x), [], inp); 
        end
        c_sweep(:,l) = mean(c_inst, 2);
    end
    
    % Generate plot of costs for different horizons
    h = figure();
    mylines = lines();
    plot(horz_sweep, c_sweep.', 'Color', 'k');
    axes = axis();
    hold on;
    horz = res_angle{1}.N_m;
    while horz >= 5
        plot([horz horz], axes(3:4), '--', 'Color', mylines(1,:), 'LineWidth', 2);
        horz = horz / 3;
    end
    fix_figure(h, [4 2], 160);
    title('Cost vs horizon for feasible initial conditions');
    xlabel('Horizon');
    ylabel('Cost');
    export_fig(['.', filesep, 'gfx', filesep, 'horz_sweep.pdf'], '-transparent');
    close(h);
end

fprintf('Constant-angle pendulum cost comparison:\n');
for k=1:length(delta_xs_ang)
    fprintf('delta-x: %s\n', mat2str(delta_xs_ang{k}));
    disp(cost_angle_comp{k}.summary);
end

%%
clear bar map_angle_ref map_inverted_ref

% Save results for later inspection
save('sols');
