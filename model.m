function [x_nn, c] = model(x_n, u_n, ~, inp)
% [x_nn, c] = MODEL(x_n, u_n, t, inp) Calculates the state
% resulting from applying inputs u_n to the system while at states x_n.
%	x_n		State at time t_n. Array where columns correspond to state
%			variables. Each row corresponds to a given state configuration.
%	u_n		Control input at time t_n. Array where columns correspond to
%			control variables. Each row corresponds to a given control 
%			configuration.
%	t		The current time
%	inp		Optional input data.
%	x_nn	State values at time t_{n+1} given the state x_n at time t_n
%			and the control u_n. Apply calculations on a row-by-row basis.
%			(IE. each row in x_n and u_n should uniquely determine each row
%			in x_nn). Should be of size size(x_n).
%	c		The cost required to move from a given row in x_n to the same
%			row in x_nn. Set to inf if state and control results in an
%			invalid/undefined system transition.

% Generate array of current states and zero-initialized cost
y = [x_n, zeros(size(x_n, 1), 1)];

% Compute state values at next sample and cost of applying control to state
y_nn = rk4(@(t,y) model_cont_time(y, u_n, inp), y, 100, [0, 0.2]);

% Finally, extract new states and costs
x_nn = y_nn(:,1:2);
c = y_nn(:,3);

    function dydt = model_cont_time(y, u, p)
        % Continuous-time model representation
        %   y   System state vector of size [n,3], where column n indicates
        %       1: pendulum angle (relative to upright position).
        %       2: pendulum angular velocity.
        %       3: accumulated cost
        %       Different rows correspond to different state configurations
        %   u   Control input signal, must be of size [n,1].
        %   p   Constant system parameters
        %
        y1 = y(:,1);
        y2 = y(:,2);
        
        dydt = [y2, ...
            -p.m.k/p.m.m * y2 - p.m.g/p.m.l * sin(y1) + 1/(p.m.m*p.m.l^2) * u, ...
            p.arg.costfun(y1, y2, u)];
    end

end