function [inp, model_params] = setup_dp(delta_x, mode)
% Set up DP solver
inp = dpm();

% Use a normalized state and small default horizon
inp.prb.N_t = 2;
inp.prb.T_s = 1;	% Solve discrete-time problem -> usa normalized "sample-rate" (i.e. sample index)

model_params = struct();

% Set up control search space.
inp.prb.U_l = -1;
inp.prb.U_h = 1;
inp.prb.N_u = 1;
inp.prb.N_u_grid = 101;

% Set up state search space.
if(strcmp(mode, 'angle'))
    inp.prb.X_l = [-1; -1];
    inp.prb.X_h = [1; 1];
elseif(strcmp(mode, 'inverted'))
    inp.prb.X_l = [-2; -1.5];
    inp.prb.X_h = [3.5; 2];
else
    error('Invalid problem type specified');
end

inp.prb.N_x = 2;
inp.prb.N_x_grid = round((inp.prb.X_h - inp.prb.X_l) ./ delta_x);

% Don't apply any initial/terminal constraints
inp.prb.X0_l = [-inf; -inf];
inp.prb.X0_h = [inf; inf];
inp.prb.XT_l = [-inf; -inf];
inp.prb.XT_h = [inf; inf];

% Set up solver

% Don't run as iterative DP solver
inp.sol.mu_grid_dec = 0.8;
inp.sol.mu_grid_inc = 1.01;
inp.sol.iter_max = 1;
inp.sol.regrid_u = false;
inp.sol.regrid_x = false;

% General setup
inp.sol.debug = false;
inp.sol.display = 'none';
inp.sol.fun_maxcombs = 1e5;
inp.sol.fun = @model;
inp.sol.cpu_parallel = false;
inp.sol.time_inv = true;
inp.sol.interpmode.x = 'linear';
inp.sol.interpmode.u = 'linear';
inp.sol.extrapmode = 'none';
inp.sol.pen_norm = 'squaredeuclidean';
inp.sol.pen_thrs = 1.5^2;
inp.sol.pen_fun_s = @(x) 1;
inp.sol.pen_fun_a = @(x) 1e3;

inp.name = mode;
inp.delta_x = delta_x;

inp.xnames = {'$\theta$', '$\dot{\theta}$'};

end

