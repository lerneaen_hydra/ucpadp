function draw_plots(s, u_law_v, x_ev, delta_x, N_m, map, opmode, varargin)

p = inputParser;
addParameter(p, 'xnames', {'$x_1$', '$x_2$'});
parse(p, varargin{:});

if(s.dp.prb.N_x == 2 && s.dp.prb.N_u == 1)
    if(N_m > 2)
        % Set control to nan for invalid states
        idx_finite = all(isfinite(cat(2,map{1}(:).c)), 2);
        u_law_v(~idx_finite) = nan;
        
        % Reshape control law to match DP grid
        u_law_grid = permute(u_law_v, [1, 3, 2]);
        u_law_grid = reshape(u_law_grid, [s.dp.prb.N_x_grid(:).', size(u_law_grid,2)]);
        
        % Draw plot of found control law
        h_f = figure(1);
        clf;
        xvals = linspace(s.dp.prb.X_l(1), s.dp.prb.X_h(1), s.dp.prb.N_x_grid(1));
        yvals = linspace(s.dp.prb.X_l(2), s.dp.prb.X_h(2), s.dp.prb.N_x_grid(2));
        contourf(xvals, yvals, u_law_grid(:,:,1).', s.dp.prb.N_u_grid, 'LineColor', 'none');
        colorbar();
        fix_figure(h_f);
        xlabel(p.Results.xnames{1});
        ylabel(p.Results.xnames{2}); 
        title(sprintf('$\\mu_0^{%d}$', N_m));
        drawnow();
        if(strcmp(opmode, 'demo'))
            export_fig(sprintf(['.', filesep, 'gfx', filesep, 'u_%s_h=%d_dx=%s.png'], s.dp.name, N_m, mat2str(s.dp.delta_x)), '-transparent', '-m3');
        end
        
        % Find delta_mu
        delta_mus = abs(u_law_grid(:,:,1) - u_law_grid(:,:,2:ceil(N_m/2)));
        delta_mu = max(delta_mus(:));
        
        % Draw plot of control law variation
        h_f = figure(2);
        clf;
        n_const = zeros(size(u_law_grid(:,:,1)));
        for j=1:size(n_const, 1)
            for k=1:size(n_const, 2)
                dummy = find(abs(u_law_grid(j,k,1) - u_law_grid(j,k,:)) < s.opts.arg.eps_u, 1, 'last');
                if(isempty(dummy))
                    n_const(j,k) = nan;
                else
                    n_const(j,k) = dummy;
                end
            end
        end
        contourf(xvals, yvals, n_const.', s.dp.prb.N_u_grid, 'LineColor', 'none');
        drawnow;
        caxis('manual');
        hold on;
        colorbar();
        if(any(n_const(:) < N_m/2))
            [~, h2] = contour(xvals, yvals, -n_const.', -[N_m/2, N_m/2], 'LineColor', 'r', 'LineWidth', 6);
            set(h2,'linestyle','none','Tag','HatchingRegion','fill','off');
            hp = findobj(h2,'Tag','HatchingRegion');
            hatchfill2(hp,'cross','LineWidth',1,'Fill','off');
        end
        fix_figure(h_f);
        xlabel(p.Results.xnames{1});
        ylabel(p.Results.xnames{2});
        title(sprintf('$\\varepsilon_\\mu$ test, $\\Delta_\\mu^{%d} = %.3g$', N_m, delta_mu));
        drawnow;
        if(strcmp(opmode, 'demo'))
            export_fig(sprintf(['.', filesep, 'gfx', filesep, 'u_delta_%s_h=%d_dx=%s.png'], s.dp.name, N_m, mat2str(s.dp.delta_x)), '-transparent', '-m3');
        end
        
        
        
        % Draw quiver plot state evolution for some initial
        % conditions
        
        % Find max delta_x
        delta_x = max(abs(delta_x), [], 1);
        
        % Number of initial conditions to display per dimension per
        % type of plot
        n_disp = [4, 15];
        
        % State evolution, decimated for evolution and quiver plots
        x_dec = cell(size(n_disp));
        % Remove some state trajectories for a less cluttered plot
        for k=1:length(x_dec)
            for i=1:length(x_ev)
                for j=1:2
                    % Make state trajectories into meshgrid format
                    x_dec{k}{i}{j} = x_ev{i}(:,j);
                    x_dec{k}{i}{j} = reshape(x_dec{k}{i}{j}, s.dp.prb.N_x_grid(:).');
                    
                    % Now remove elements from arrays
                    stepsize = max(1, floor(size(x_dec{k}{i}{j}) ./ (n_disp(k) - 1)));
                    x_dec{k}{i}{j} = x_dec{k}{i}{j}(1:stepsize(1):end, 1:stepsize(2):end);
                end
            end
        end
        
        
        % Reformat state trajectories into original format for
        % plotting
        x_ev_plot = cell(size(x_dec));
        for k=1:length(x_ev_plot)
            for i=1:length(x_dec{j})
                for j=1:length(x_dec{k}{1})
                    x_ev_plot{k}(:,j,i) = x_dec{k}{i}{j}(:);
                end
            end
            % Remove initial conditions that eventually leave
            % permissible state range
            x_ev_plot{k} = x_ev_plot{k}(all(all(isfinite(x_ev_plot{k}), 2),3),:,:);
        end
        
        
        h_f = figure(3);
        clf;
        
        % Generate quiver plot
        quiver(x_ev_plot{2}(:,1,1), x_ev_plot{2}(:,2,1), x_ev_plot{2}(:,1,2) - x_ev_plot{2}(:,1,1), x_ev_plot{2}(:,2,2) -  x_ev_plot{2}(:,2,1));
        
        hold on;
        mylines = lines();
        
        % Generate trajectory plot
        for k=1:size(x_ev_plot{1}, 1)
            plot(squeeze(x_ev_plot{1}(k,1,:)), squeeze(x_ev_plot{1}(k,2,:)), 'color', mylines(2,:));
        end
        
        % Generate marker for terminal states
        scatter(x_ev_plot{1}(:,1,end), x_ev_plot{1}(:,2,end), 'MarkerEdgeColor', mylines(2,:), 'MarkerFaceColor', 'none');
        
        % Plot threshold region for termination
        % Find average terminal state
        x_avg = mean(x_ev_plot{1}(:,:,end), 1);
        
        % Compute vertices for termination region
        thrs = [x_avg(:) + s.opts.arg.eps_x, x_avg(:) - s.opts.arg.eps_x];
        plot([thrs(1,1), thrs(1,2), thrs(1,2), thrs(1,1), thrs(1,1)], [thrs(2,1), thrs(2,1), thrs(2,2), thrs(2,2), thrs(2,1)], ...
            'Color', mylines(3,:), 'LineWidth', 1.5);

        
        axis([s.dp.prb.X_l(1), s.dp.prb.X_h(1), s.dp.prb.X_l(2), s.dp.prb.X_h(2)])
        fix_figure(h_f);
        title(sprintf('$\\varepsilon_x$ test, $\\Delta_x^{%d} =$ %s', N_m, mat2str(delta_x.', 3)));
        xlabel(p.Results.xnames{1});
        ylabel(p.Results.xnames{2});
        drawnow;
        if(strcmp(opmode, 'demo'))
            export_fig(sprintf(['.', filesep, 'gfx', filesep, 'cl_%s_h=%d_dx=%s.pdf'], s.dp.name, N_m, mat2str(s.dp.delta_x)), '-transparent');
        end
    end
else
    fprintf('Control law visualization only generated for system with two state variables and one control variable\n');
end
