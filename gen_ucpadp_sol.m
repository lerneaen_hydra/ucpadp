function res = gen_ucpadp_sol(dp_param, mod_const, varargin)
% Apply the UCPADP method

% Adjust DP tolerances based on input
p = inputParser;
addParameter(p, 'eps_x', 2*(dp_param.prb.X_h - dp_param.prb.X_l)./dp_param.prb.N_x_grid);	% Convergence tolerance for state, set to separation between DP grid points
addParameter(p, 'eps_u', 2*(dp_param.prb.U_h - dp_param.prb.U_l)./dp_param.prb.N_u_grid);	% Convergence tolerance for control, set to separation between DP grid points
addParameter(p, 'max_horz', 2048);	% Maximum horizon length
addParameter(p, 'init_horz', 5);    % Initial horizon, allow for manual override
addParameter(p, 'lambda', 0);
addParameter(p, 'costfun', []);     % Optionally supply a cost function that model function can use
addParameter(p, 'silent', false);   % Optionally suppress all text and plot outputs
parse(p, varargin{:});

ucpadp_inp.opts.m = mod_const;

% Use the DPM library and use it as a "standard" DP solver (i.e. not using
% any IDP-MP functionality as described in the associated article, see
% ./dpm/dpm_article.pdf)
if(exist([pwd filesep 'gen_ucpadp_sol.m'], 'file'))
    % Add path to DPM if executing example, if executing from other
    % directory DPM path and hatchfill2 path must be manually added.
    ucpadp_inp.opmode = 'demo';
    addpath([pwd filesep 'dpm' filesep 'dpm']);
else
    ucpadp_inp.opmode = 'normal';
end

% Apply UCPADP method to get stationary control law, closed-loop system
% behavior, time-varying control law, and minimum horizon
ucpadp_inp.dp = dp_param;
ucpadp_inp.opts.arg = p.Results;

[u_law, x_ev, u_map, N_m] = ucpadp(ucpadp_inp);

% Store results in nicely formatted structure
res.u_law = u_law;
res.x = x_ev;
res.N_m = N_m;
res.u_map = u_map;


    function [u_law_f, x_ev, u_law_v, N_m] = ucpadp(s)
        % Rudimentary UCPADP implementation
        
        % Returns:
        %   u_law   The stationary control law
        %   x_cl    The closed-loop state evolution for a wide range of
        %           initial conditions.
        %   u_map   The time-varying control law, for samples
        %           N-\tilde{N}_m to N.
        %   c       The total cost from all tested initial conditions
        %   N_m     The detected minimum horizon \tilde{N}_M
        
        
        % Here a standard DP-solver is used that doesn't easily allow for
        % performing successive backward/forward iterations based on
        % previous calls. A numerically inefficient way of resolving this
        % is to simple re-solve the problem with successively doubled
        % horizon lengths. This results in approximately doubling the
        % computational time needed to solve the problem, but gives
        % otherwise identical results.
        
        % Initialize search horizon (default of two samples)
        N_m = s.opts.arg.init_horz;
        
        % Infinite loop, will break out of when horizon length is
        % sufficiently long.
        if ~s.opts.arg.silent
            fprintf('Generating UCPADP solution.\n');
        end
        while(true)
            if ~s.opts.arg.silent
                fprintf('Testing with horizon length %d\n', N_m);
            end
            
            % Update problem horizon
            s.dp.prb.N_t = N_m;
            
            % Perform N_h back-calculation steps to get a control law
            [u_law_f, u_law_v, x0s, ~, c, map] = calc_u_law(s);
            
            % Check if N_h back-calculation steps were sufficient (i.e. all
            % initial conditions have converged to stationary value and
            % control was similar enough during entire interval).
            
            % As it's computationally cheaper to compare the control law,
            % a fast choice would be to first perform this test and only
            % progess to compare the state evolution if the control law is
            % stationary. However, here we will not do this in order to
            % generate some useful plots.
            
            % Indices of initial conditions that stay in the permitted
            % state space range.
            idx_valid = isfinite(c);
            
            % Check if all control that stay within search space are within eps_u from one another
            delta_u = u_law_v(idx_valid,:,1) - u_law_v(idx_valid,:,1:ceil(N_m/2));
            u_conv = abs(delta_u) < s.opts.arg.eps_u(:).';
            
            % The control law was near-constant, so test for
            % convergence. Apply the near-stationary control law N_h/2
            % times to all tested initial conditions and check that the
            % norm of the initial values is eventually small enough.
            
            % Here, store the history of the state evolution in order to
            % make some plots. This is not necessary for the method.
            x_ev = cell(1,ceil(N_m/2));
            x_ev{1} = x0s;
            
            for i = 2:length(x_ev)
                x_ev{i} = s.dp.sol.fun(x_ev{i-1}, u_law_f(x_ev{i-1}), 0, s.opts);
            end
            
            idx_valid = all(isfinite(x_ev{end}),2);
            
            % Find termination criterion delta_x
            delta_x = x_ev{end}(idx_valid,:) - mean(x_ev{end}(idx_valid,:));
            x_conv = abs(delta_x) < s.opts.arg.eps_x(:).';
            
            if ~s.opts.arg.silent
                % Draw plot of the state evolution
                draw_plots(s, u_law_v, x_ev, delta_x, N_m, map, s.opmode, 'xnames', s.dp.xnames);
            end
            
            if(all(x_conv(:)) && all(u_conv(:)))
                % All initial values have converged and the control law was
                % near-constant, we're done!
                break;
            end
            
            %If we get there, the current horizon was too short
            % Increase horizon and try again. Simulate the horizon lengths
            % given by performing an additional 2*N_m backward calculation
            % iterations.
            N_m = N_m + N_m * 2;
            
            if(N_m > s.opts.arg.max_horz)
                % Horizon too long, quit
                break;
            end
        end
        % Return u_law, x_cl, u_map
        return
    end

    function [u_law_f, u_law_v, x0s, x_vec, c, map] = calc_u_law(s)
        % Calculate the control law for the given problem
        %   u_law_f     Function handle that returns the control law for a
        %               given state row vector.
        %   u_law_v     Control law values for each sample, evaluated at
        %               grid points
        %   x0s         State configurations evaluated by DP method
        %   c           Total cost of each associated initial condition
        %   map         Complete map of DP results
        
        % Apply DP
        [~, grd, ~, ~, map] = dpm(s.dp, s.opts);
        
        % Return entire history of control laws to compare for equality
        u_law_v = zeros([size(map{1}(1).u_o), length(map{1})]);
        for i=1:size(u_law_v, 3)
            u_law_v(:,:,i) = map{1}(i).u_o;
        end
        
        % Extract timehopefully stationary control law, using interpolation
        % to approximate optimal control between tested grid points.
        x_vec = cell(1, s.dp.prb.N_x);
        for i=1:s.dp.prb.N_x
            x_vec{i} = linspace(s.dp.prb.X_l(i), s.dp.prb.X_h(i), s.dp.prb.N_x_grid(i));
        end
        
        % Generate N_u cell array with optimal control law 
        u_law_f_int = cell(s.dp.prb.N_u, 1);
        
        for i=1:length(u_law_f_int)
           u_dummy = reshape(u_law_v(:,i,1), s.dp.prb.N_x_grid(:).');
           
           % Make control law NaN for values that do not stay in search range
           c_v_grid = reshape(map{1}(1).cum, s.dp.prb.N_x_grid(:).');
           u_dummy(~isfinite(c_v_grid)) = nan;
           
           F = griddedInterpolant(x_vec, u_dummy, 'linear', 'none');
           
           u_law_f_int{i} = @(x) F(x);
        end
        
        % Generate final control law, evaluate u_law_f_int with identical
        % argument for all cell elements
        u_law_dummy = @(v) cellfun(@(x) x(v), u_law_f_int, 'un', false);
        u_law_f = @(v) reshape(cell2mat(u_law_dummy(v)), [], s.dp.prb.N_u);
        
        % Return the tested state values
        x0s = grd{1}.x;
        c = map{1}.c;
    end
end
