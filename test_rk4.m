% Simple testbench for rk4 solver

% Generate reference solution with ode45
% Solve y' = y, y(0) = 1 for range t = [0, 1]
[t2, y2] = ode45(@(t, y) y, [0, 1], 1);

% Solve using RK4 methods
% For reasonableness, use the same number of data points in RK4 solution
N = length(t2);
[y1, t1] = rk4(@(t, y) y, 1, N, [0, 1], 'log');
y1 = cell2mat(y1);

% Compare solutions
y2_interp = interp1(t2, y2, t1);

eps = norm(y1 - y2_interp)/N;

fprintf('\n\nTest results:\n');

fprintf('RK4 and ODE45 solutions differ by %g\n\n', eps);

time1 = timeit(@() rk4(@(t, y) y, 1, N, [0, 1]));
time2 = timeit(@() ode45(@(t, y) y, [0, 1], 1));

fprintf('RK4 took %e seconds\nODE45 took %e seconds\nrelative speed-up of %g (for single-state single-initial-condition test)\n\n', time1, time2, time2/time1);

% Test rk4 with a multi-state multi-IC problem

y0 = [1, 0; 0.2, 0.2];
[y, t] = rk4(@(t, y) harm_osc(y), y0, 1e3, [0, 2*pi], 'log');
y = cat(3, y{:});

y1 = squeeze(y(1,:,:));
y2 = squeeze(y(2,:,:));

clf;
plot(t, [y1; y2]);

legend('IC1 y', 'IC1 y''', 'IC2 y', 'IC2 y''');

function dydt = harm_osc(y)
	y1 = y(:,1);
	y2 = y(:,2);
	dydt = [-y2, y1];
end